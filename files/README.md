# Konch Build Files

Here you can find all the source files to build your own Konch. Use this part of the repository if you want to suggest improvements. The files have two main categories.

- Main Structure
- Electronics

## Main Structure

The CAD design of the main structure can be found in the KonchMain.FCStd FreeCAD project file. Please use [FreeCAD 0.18.4](https://github.com/FreeCAD/FreeCAD/releases/tag/0.18.4) as the design seems to break in later versions.

The main structure also includes 3D printable fixtures for electronics. Electronics should be put in a box that is both: waterproof and fireproof. IP rated box is probably a good start.

## Electronics

There are several electronics addons for Konch. Most of the parts are needed for the transceiver. There is also a LED light to make it usable for reading in the dark.

- Raspberry Pi Model 3B+ (recommended)
- USB audio dongle with mic-in and line-out.
- Microphone (self made)
- Amplifier
- Speaker
- LED light

The ambition of the project is to make it possible to manufacture most of the parts in a fab lab.

### Microphone

The main reason for the microphone to be self made is that it should be well integrated in the main structure. Of course you can use a lavalier microphone as an alternative.
