## Konch Website

[Hugo](https://gohugo.io/) is used to build the website of the project. You can find configuration, content and template files in the [website](website) directory. HTML website is built and published using GitLab CI process described in [.gitlab-ci.yml](.gitlab-ci).

To develop locally, make sure `hugo` is installed and proceed with the following steps in Terminal (assuming that your current working directory is `/path/to/furnish-not-19` already).

```
cd website
hugo serve
```

This will build the website and launch a HTTP server on your machine locally.
