+++
Title = "Konch"
SubTitle = "Open-Source Multipurpose Urban Design Element"
Description = "Konch is an open-source multipurpose urban design element that can be made in a standard fab lab."
Author = "Krisjanis Rijnieks"

[Repository]
  URL = "https://gitlab.com/kriwkrow/konch"
  CallToAction = "Access Repository"
+++

## About the Project

Konch is part of the [FURNISH](http://furnish.tech) project. Konch is a multipurpose object enabling discussion or relaxation in the Covid-19 situation. It’s a comfortable seat with an integrated audio system. This way one can keep the social distance recommendations and still have a vivid, organic, screen-less conversation, as a balance to the recently so popular video chats.

Konches can be easily arranged in any constellation up to a 100 meters apart, which is the area covered by the integrated router. The platform is suitable for both outdoor, and indoor setting. Because of its weatherproof coating and its shell-like design, it can be a cosy hideout during cloudy and windy days. It’s perfect for students debating from different corners of the school/university, or simply enjoying a chat over a cup of coffee with some fresh air.

When the classrooms and cafés are a safety risk, we invite people to take conversations outdoors. No screens in-between, instead – social distance with audio connection. Laugh, argue and tinker without worries!

## Our Team

**[Krisjanis Rijnieks](https://rijnieks.com)** is a digital fabrication expert, designer and artist. He is managing the Aalto Fablab which is a part of the Aalto Studios at the Aalto University. As a Fab Academy graduate he is organising and instructing the Fab Academy at the Aalto Fablab. He is the author of the open-source projection mapping software ofxPiMapper.

**Ranjit Menon** is an interaction designer and artist (MA Sound in New Media from Aalto University). He is currently finishing Fab Academy 2020. He works in service and interactive design, audio-visual design, live sound and music, theatre, human factors and usability engineering. He has teaching experience in Sound Design and Music to Systems Thinking.

**[Dorota Orlof](https://dorkastrong.com)** is a Visual Storyteller. She is a Graphic Design MA graduate from the Academy of Fine Arts in Cracow, and a Fab Academy alumni from the Institute of Advanced Architecture in Barcelona. She was part of The Hive Collaborative Residency in Aix-en-Provence, where she co-founded [Telenatura Lab](https://telenaturalab.com) collective.

**[Leda Vaneva](https://ledavaneva.com)** is a multidisciplinary artist and designer based in Helsinki. She has been implementing digital fabrication in her art pieces, creating objects and installations. She holds MA degrees in New Media (Aalto University, FI) and Photography (National Academy of Arts, BG), and is part of the Finnish art associations MUU and AV-Arkki.

## Konch in Action

![](images/gallery-1.jpg)
![](images/gallery-2.jpg)
![](images/gallery-3.jpg)
![](images/gallery-4.jpg)
![](images/gallery-5.jpg)
![](images/gallery-6.jpg)

## Build your Own!

If you like what you see and you want to build your own version of Konch, go to [the repository](https://gitlab.com/kriwkrow/konch) and read the instructions there. Open an [issue](https://gitlab.com/kriwkrow/konch/-/issues) if you have questions, comments or suggestions. Pull requests are welcome too!
