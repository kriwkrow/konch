# Audio Development

Documentation regarding audio research. As a log of actions first. We are looking for a fast and stable (enough) solution for one-to-many audio transmission. Preferably wireless. At the point of writing this the idea is to use a high speed WiFi network and Raspberry Pi minicomputers. Each Raspberry Pi would have a speaker and microphone connected. Microphone would capture audio and broadcast it to all other Raspberry Pi's in the network.

## Linux Software Research

To begin with, keywords such as "linux walkie talkie" were used. Here is what came up.

https://talkkonnect.com/  
talKKonnect is a headless self contained mumble Push to Talk (PTT) client with a mobile transceiver form factor complete with LCD, Channel and Volume control.

https://github.com/dchote/talkiepi/
talkiepi is a headless capable Mumble client written in Go.

https://www.mumble.info/
Mumble is a free, open source, low latency, high quality voice chat application.

Each of the above led to one another in the order above.

Installing Mumble Server on Ubuntu Linux.

```
sudo add-apt-repository ppa:mumble/release
sudo apt update
sudo apt install mumble
sudo apt install mumble-server
sudo dpkg-reconfigure mumble-server
```

The configuration interface will appear. No run on boot was selected. The name of the server daemon is `murmurd`.

It was possible to install Mumble client on Ubuntu Linux and iPhone. What about Raspberry Pi? After installing a Raspberry Pi OS Lite, it was not possible to find the mumble client in the apt repositiories. 

One option would be to build it from scratch, but then anyway it would need a GUI and that is what we want to avoid. `xvfb` is an option, but it would be more of a hack.

It seems that this is where the abovementioned talkiepi starts to make sense. It is built to be used on a Raspberry Pi OS Lite and basically has all the functionality. Not sure if the button is needed, but let's see.

After following the `talkiepi` README, receiving on Pi from Ubuntu Linux worked very well. There were things that were not clear. 

1. When editing the /etc/systemd/system/mumble.service, you have to specify the port of the mumble server. The default port is `64738`  
   ```
   ExecStart = /home/mumble/bin/talkiepi -server 192.168.1.105:64738 -username pi
   ```
2. You should make sure that the audio interfaces are configured properly. Use `aplay -l` to list your audio devices. My USB capture card starts with the name **Device**. The output looks as follows and you should pick the name after `card N:` and before `[`.
  ```
  card 2: Device [USB Audio Card and what not]
  ```

3. Create talkiepi-speciffic environment variable file with `sudo nano /etc/profile.d/talkiepi.sh`. The contents of the file should be as follws. Save with `Ctrl + X` and `Enter`.
  ```
  export ALSAPCM=Device
  ```

4. Set up ALSA config file with `sudo nano /etc/asound.conf`. Add the following contents.
  ```
  pcm.!default {
     type plug
     slave.pcm {
        @func getenv
        vars [ ALSAPCM ]
        default "hw:Device"
     }
  }
  ```
  