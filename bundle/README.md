Project name: Konch
Team: NOT-19 (Aalto Fablab)
Members: Krisjanis Rijnieks, Leda Vaneva, Dorota Orlof, Ranjit Menon
Date: 14 December 2020
Website: https://konch.info
Lincense: Please consult LICENSE.txt

Description
===========
Konch is part of the Furnish project (https://furnish.tech). Konch is a multipurpose object enabling discussion or relaxation in the Covid-19 situation. It’s a comfortable seat with an integrated audio system. This way one can keep the social distance recommendations and still have a vivid, organic, screen-less conversation, as a balance to the recently so popular video chats. Konches can be easily arranged in any constellation up to a 100 meters apart, which is the area covered by the integrated router.

Included Files
==============
- KonchMain.FCStd: Main design file of the object. FreeCAD version 0.18.4 was used to create the design. FreeCAD is free and open source software and it was chosen to make it possible to tweak the design by as many people as possible.
- KonchMain.step: The frozen design state of the shell. Konch consists of a main wooden structure and optional electronic parts. Consult the STEP file to see how parts fit together.
- Photos: Consult the Photos directory to see that the final objects have been actually assembled in case you are unsure.
- Drawings: In this directory you can find drawing files that can be used to produce paths for CNC milling. We also added a VCarve project file.
- 3D Printing: In this directory you can find all the parts that have been designed for 3D printing.
- Macros: In this directory you can find macro scripts that will be useful if you choose to tweak the FreeCAD design file. You can import them using the Macro / Macros menu in FreeCAD.
- Electronics: Files related to the electronics production of the project.
- LICENSE.txt: License terms of this repository.
- README.txt: The file you are reading now.

Materials
=========
We chose to go with 18mm thick pine plywood sheets. Each sheet that we used is 2240x1220mm in dimensions. In order to fit our CNC machine with the machining surface that of 1300x1200, we had to cut the plywood sheets in half.

For each Konch you will need 3.5 2240x1220mm plywood sheets.

CNC Milling
===========
We used 6mm 2-flute downcut tool to cut the parts. A downcut tool helps to prevent chipping of the top layer of the sheet. 16000 rpm was the spindle speed. We used the following formula to calculate the horizontal feedrate.

fr = nfl * chl * rpm

fr: feedrate
nfl: number of flutes
chl: chip load, mm per revolution
rpm: revolutions per minute

The resulting unit of the feedrate is going to be mm/min (millimeters per minute). Make sure that your path generator is set to the same.

Mitre Angles
==============
To make the multiple parts fit "seamlessly" we combined the Macro and Spreadsheet functionality of FreeCAD. Please use the Angles.png file to see the mitre angles to be beveled.

On the left column of the table you can see the names of the planes between which the angle has been measured. "Pentagon Angles" stands for the mitre angles between the walls surrounding the pentagonal base. For example, "Roof Left, Wall Left" means that the angles have been measured between the left roof and left wall if you look at the object from the front. If you are unsure about the naming of the walls, consult the KonchMain.step file or use FreeCAD to open the KonchMain.FCStd project.

The "Actual Angle" shows the actual angle between the surfaces. "Bevel Angle" is half of that as each face to be joined covers half of the actual angle. The "Grinder Angle" is the angle to be used for the angle of a circular saw. We used a hand driven, portable circular saw that we moved along a rail that was put on top of the surface in question. The edges of the rail and the surface should match.

The "Biscuit Angle" is used to set the angle of a biscuit joiner machine, such as Lamello. Feel free to place the biscuits the way you feel it being right.

If you make changes to the original KonchMain.FCStd project file, make sure to use the Macros / Update Angles.FCMacro macro to update the Angles spreadsheet.

3D Printing
===========
We used PLA 2.85mm filament to print all the parts with different 3D printers. We used Cura Ultimaker to print with the Ultimaker printers and Cura LulzBot to print with LulzBot printers at the lab.

0.15mm layer height was used in most cases with 20% infill. The design of the 3D printable elements was made to avoid the use of supports while printing. You should be in the best position to decide what filament, printer and settings to use in order to get the best results in your lab.

Below is the list of 3D printable files along with explanation how they are used in the element.

- SupportPad.stl: Each Konch should have 5 of these. One for each of the 5 corners of the pentagonal structure.
- LedFixtureProfile.stl: 10cm long segment that fits between roof planes of the element. The more you print, the longer the LED lightning element is going to be. We used 4 profile segments to achieve 40cm long LED light to be mounted into the ceiling of the element.
- LedFixtureCap.stl: This is a single element to be printed for each lamp to be made. Use 2 - 3mm diameter screws to attach it to the ceiling.
- LedFixtureConnector.stl: This should house the connector part of the 12V LED power supply connection.
- MicCableFixture.stl: Small helper element to fix and mask the microphone cable.
- StrainRelief.stl: Strain relief element to match the custom omnidirectional microphone design that can be found in the Electronics directory.

Electronics
===========
Each of the Konch elements is supposed to be connected to others via a voice chat system. At the core of the system there is a 5G WiFi network. Each element has a Raspberry Pi minicomputer that connects to the WiFi network with its built-in WiFi antenna.

Raspberry Pi Setup
==================
Raspberry Pi 3 model B+ was used for the current implementation of the Konch. We used the Raspberry Pi OS Lite as the base system for each of the units. You can download it from https://www.raspberrypi.org/software/ - follow the instructions from the website to install it on your SD card which should be at least 8GB Class 10 MicroSD card that fits into the SD card slot of your Pi.

First is to boot up your Pi with a display and keyboard connected. Use the username "pi" and password "raspberry" to log into the console. Use "sudo raspi-config" command to access the configuration screen of your Raspberry Pi unit.

- Change the hostname to something like "Konch 01".
- Connect to a WiFi network.

Use the config application to connect your Pi to a known WiFi network with a working internet connection. It will be needed to install all the software packages. Start with pulling latest operating system updates with the following commands.

> sudo apt update
> sudo apt upgrade

Mumble is our choice in terms of the voice chat system. It is open source, free and actually the best voice chat system that is available on the market. Website: https://www.mumble.info/ Continue with installing the mumble server which should be installed only on one of the Raspberry Pi computers.

You should install mumble server only on one Konch Raspberry Pi unit.

> sudo apt install mumble-server

Next, we will use a version of the mumble client - talkiepi. It is a software overlay designed to make a walkie-talkie out of a Raspberry Pi. Doing it with the Raspberry Pi using a WiFi network gives many advantages over using a traditional intercom system or a transceiver. With WiFi you can use encryption of your voice messages if needed.

You can access talkiepi repository by following this link https://github.com/dchote/talkiepi To make your life easier, below you can find an essence of installing it. Remeber to execute these lines one by one and read the return messages carefully.

Install talkiepi for all Konch Raspberry Pi units.

Add mumble user
---
> sudo -i
> adduser --disabled-password --disabled-login --gecos "" mumble
> usermod -a -G cdrom,audio,video,plugdev,users,dialout,dip,input,gpio mumble

Install dependencies
---
> su pi
> sudo apt install golang libopenal-dev libopus-dev git -y

Configure GoLang environment as mumble user
---
> su mumble
> mkdir ~/gocode
> mkdir ~/bin
> export GOPATH=/home/mumble/gocode
> export GOBIN=/home/mumble/bin

Download talkiepi and its dependencies
---
> cd $GOPATH
> go get github.com/dchote/gopus
> go get github.com/dchote/talkiepi

Compile and install talkiepi
> cd $GOPATH/src/github.com/dchote/talkiepi
> go build -o /home/mumble/bin/talkiepi cmd/talkiepi/main.go

You will need to configure talkiepi to start and connect to a mumble server on every reboot of the Raspberry Pi. During actual setup of the element there will be many times you will connect and disconnect the element from a power supply. It would be a pain to connect a keyboard and monitor every time that happens.

Before we do that, you should set up the Konch 5G network with the WiFi router available to you. When that is done, connect to the newly created nework with each of the Raspberry Pi computers separately. You should write down the IP address of the main Raspbery Pi where you installed the mumble server. Use the following command to learn it.

> hostname -I

If nothing shows up, the WiFi connection has not been successful. Double check that DHCP functionality has been enabled on the router.

Pro Tip: you can map MAC addresses of the Raspberry Pi units to static IP addresses on the WiFi router. It is out of the scope of this documentation to describe how it works, but it would be best if you could contact someone who knows how to set up LAN networks.

Once you know the IP address of the server, you can set up each of the Raspberry Pi computers separately to connect to the server.

Edit the mumble.service file to point to your server.
---
> nano /home/mumble/gocode/src/github.com/dchote/talkiepi/conf/systemd/mumble.service

Change the line ExecStart = /home/mumble/bin/talkiepi to:
ExecStart = /home/mumble/bin/talkiepi -username <username> -server <mumble_server_ip>:64738
Press ctrl+x, type y and hit enter to save and close the file.

Enable mumble client
---
> sudo cp /home/mumble/gocode/src/github.com/dchote/talkiepi/conf/systemd/mumble.service /etc/systemd/system/mumble.service
> systemctl enable mumble.service

Et voila, you can reboot and it should be working.

ONLY IF you have configured your microphone and speaker in the right way.

TBC
