EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:R R1
U 1 1 5FA50D3D
P 6775 4325
F 0 "R1" H 6845 4371 50  0000 L CNN
F 1 "150" H 6845 4280 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6705 4325 50  0001 C CNN
F 3 "~" H 6775 4325 50  0001 C CNN
	1    6775 4325
	1    0    0    -1  
$EndComp
$Comp
L fab:CP C1
U 1 1 5FA5186D
P 6375 4325
F 0 "C1" H 6257 4279 50  0000 R CNN
F 1 "3.3uF" H 6257 4370 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 6375 4325 50  0001 C CNN
F 3 "" H 6375 4325 50  0001 C CNN
	1    6375 4325
	-1   0    0    1   
$EndComp
$Comp
L Device:Microphone MK1
U 1 1 5FA51FF1
P 6175 4850
F 0 "MK1" V 5908 4850 50  0000 C CNN
F 1 "Microphone" V 5999 4850 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 6175 4950 50  0001 C CNN
F 3 "~" V 6175 4950 50  0001 C CNN
	1    6175 4850
	0    1    1    0   
$EndComp
Text Label 6875 4050 0    50   ~ 0
3V
Wire Wire Line
	6375 4850 6375 4600
Wire Wire Line
	6375 4600 6775 4600
Wire Wire Line
	6775 4600 6775 4475
Connection ~ 6375 4600
Wire Wire Line
	6375 4600 6375 4475
Wire Wire Line
	5975 4850 5800 4850
Wire Wire Line
	5800 4850 5800 3850
Wire Wire Line
	7100 4050 6775 4050
Wire Wire Line
	6775 4050 6775 4175
Wire Wire Line
	7100 3950 6375 3950
Wire Wire Line
	6375 3950 6375 4175
Text Label 6600 3950 0    50   ~ 0
SIG
Wire Wire Line
	5800 3850 6925 3850
Text Label 6350 3850 0    50   ~ 0
GND
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5FC282EA
P 7300 3850
F 0 "J2" H 7328 3826 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7328 3735 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7300 3850 50  0001 C CNN
F 3 "~" H 7300 3850 50  0001 C CNN
	1    7300 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6925 3850 6925 3750
Wire Wire Line
	6925 3750 7100 3750
Connection ~ 6925 3850
Wire Wire Line
	6925 3850 7100 3850
$EndSCHEMATC
