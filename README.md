# Konch

![Konch Hero by Leda Vaneva](Konch_Hero_Leda-Vaneva.jpg)

Konch is an open source multipurpose urban design element that started as a part of the [FURNISH](https://furnish.tech/) (Fast Urban Responses for New Inclusive Spaces and Habitat) project. It was designed with the COVID-19 crisis in mind and the initial idea was to use it to promote social distancing during the pandemic. The built-in radios would allow people to have playful interaction with each other while being in the same space and maintaining a safe distance.

Konch is designed to be build in a standard fab lab. [Aalto Fablab](https://fablab.aalto.fi) is the place it was built first. This repository contains all the source files and instructions needed to build your own. It is still in its early stages and it is possible that something is incomplete. Do not hesitate to open an issue to ask a question or suggest ideas.

- [Access website](https://kriwkrow.gitlab.io/furnish-not-19/)

## Structure

- All the design files of the project you can find in the [files](files) directory.
- A human readable bundle including manufacturing instructions can be found in the [bundle](bundle) directory.
- Website related files can be found in the [website](website) directory.

## Contributing

The project is in its early stages and there are things missing. Do not hesitate to open an issue and ask questions if you are about to build your own Konch. Suggestions and ideas are welcome. If you make improvements in the source files, consider submitting a pull request.

## License

© Krisjanis Rijnieks, Leda Vaneva, Dorota Orlof and Ranjit Menon

Creative Commons - Attribution-NonCommercial  
Lets others use and build on your design non-commercially, as long as they credit you.  
For full details, visit https://creativecommons.org/licenses/by-nc/4.0/
